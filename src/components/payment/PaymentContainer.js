import React from 'react'
import Banner from '../Banner'
import PaymentList from './PaymentList'
import { useDispatch } from "react-redux";
import {setActiveTab} from '../store/NavActions'
const PaymentContainer = () => {
    const dispatch = useDispatch();
    dispatch(setActiveTab(1))
     return(
        <>
            <Banner />
            <PaymentList />
            
        </>
    )
}

export default PaymentContainer