import React,{useState} from 'react'
import './css/SearchFilter.css'
const Filter = (props) => {
    const [searchValue, setSearchValue] = useState("")
    const [searchBy, setSearchBy] = useState("")
    const [message, setMessage] = useState("")

    const handleFilter = () =>{
        if(searchBy && searchValue)
        {
           const postFilterData = props.data.filter(item => item[searchBy].toString() === searchValue)
           props.applyFilter(postFilterData)
        }else
            setMessage("Error! Provide data for all fields")
    }

    const clearFilter = () =>{
       props.applyFilter(props.data)
       setSearchValue("")
       setSearchBy("")
       setMessage("")
       
    }

    const handleOnChange = (value, setState) => {
        setState(value)
        setMessage("")
    }

    return (
        <div className="container text-center form-inline ">
               <div className="form-group row">
                    <select id="searchBy" className="form-control m-2" value={searchBy} onChange={(e) => handleOnChange(e.target.value,setSearchBy)}>
                        <option default></option>
                        <option value="id">ID</option>
                        <option value="type">Type</option>
                        <option value="custID">custID</option>
                        <option value="amount">Amount</option>
                        <option value="paymentDate">Date</option>
                    </select>

                    <input value={searchValue} onChange={(e) => handleOnChange(e.target.value,setSearchValue)} className="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search" />
                    <button className="btn btn-outline-success m-2 my-sm-0" type="submit" onClick={()=>handleFilter()}>Apply</button>
                    <button className="btn btn-outline-success m-2 my-sm-0" type="submit" onClick={()=>clearFilter()}>Clear</button>
                    {message!==""?<span className="m-3 text-danger">
                            <strong>{message} </strong> 
                    </span>:null}
                </div>
         
        </div>


    )
}

export default Filter;
