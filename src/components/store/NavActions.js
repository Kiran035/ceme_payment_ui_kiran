import {ACTIVE_TAB as CONSTANTS} from '../constants/Constants'



export const setActiveTab = (index) =>{
    
    return{
        type:CONSTANTS.ACTIVE_TAB,
        data:index
    }
}