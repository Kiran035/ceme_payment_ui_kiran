import React from 'react'
import { Route, BrowserRouter as Router } from 'react-router-dom'
import PaymentContainer from '../payment/PaymentContainer'
import AddPaymentForm from '../payment/AddPayment'
import About from '../about/About'
import NavBar from '../NavBar'

const AppRouter = () => {
    return (
        <>
            <Router>
                <NavBar />
                <div className="text-center">
                    <Route exact path="/" component={PaymentContainer} />
                    <Route exact path="/addPayment" component={AddPaymentForm} />
                    <Route exact path="/about" component={About} />
                </div>
            </Router>

        </>


    )
}

export default AppRouter