import {combineReducers} from 'redux'
import {paymentReducer} from './PaymentReducers'
import {statusReducer} from './StatusReducers'
import {navReducer} from './NavReducers'

const reducer = combineReducers({
    payment: paymentReducer,
    status: statusReducer,
    navMenu:navReducer,
})


export default reducer