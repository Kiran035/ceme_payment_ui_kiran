import React from 'react'

const Banner = () =>{
    return(
        <section className="jumbotron text-center">
            <div className="container">
            <h1>Payment example</h1>
            <p className="lead text-muted">Something short and leading about the collection below—its contents, the creator, etc. Make it short and sweet, but not too short so folks don’t simply skip over it entirely.</p>
            <p>
                <a href="#" className="btn btn-primary my-2">Backend Source Code</a>
                <a href="#" className="btn btn-secondary my-2">UI Source Code</a>
            </p>
            </div>
        </section>
    )
}

export default Banner