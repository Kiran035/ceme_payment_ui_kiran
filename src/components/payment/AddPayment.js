import React from 'react'
import { connect } from 'react-redux'
import {addPaymentDetails, addPaymentDetailsBegin} from '../store/PaymentActions'
import {setActiveTab} from '../store/NavActions'
class AddPaymentForm extends React.Component
{
    constructor(props){
        super(props)
        const { dispatch } = this.props;
        dispatch(addPaymentDetailsBegin())
        dispatch(setActiveTab(2))
        this.state ={
            id:"",
            paymentDate:"",
            type:"",
            custID:"",
            amount:"",
            message:""

        }

    }

    validation = () =>{
        return (this.state.id && this.state.paymentDate && this.state.type && this.state.custID && this.state.amount)
    }

    handleOnSubmit = (e) =>{
        e.preventDefault();
        if(this.validation()){
            try {
                const { dispatch } = this.props;
                dispatch(addPaymentDetails(this.state)).then(
                    (res) => this.props.history.push('/'),
                    (err) => this.setState({message:this.props.error.message})
                )
                
            }catch(error){}
        }else
            this.setState({message:"Require data for all fields"})
		
    }

    handleOnChange = (event) =>{
        const id = event.target.id;
        const value = event.target.value;
        this.setState({[id]:value, message:""})
    }
    render(){
           return(
            <form className="mt-5" onSubmit={(event) => this.handleOnSubmit(event)}>
               
               <div className="form-group row">
                    <label htmlFor="id" className="col-sm-1 col-form-label">Id</label>
                    <div className="col-sm-5">
                    <input type="number" onChange={(e) => this.handleOnChange(e)} id="id" value={this.state.id} className="form-control" placeholder="Id" />
                    </div>
                </div>

                <div className="form-group row">
                    <label htmlFor="paymentDate" className="col-sm-1 col-form-label">Date</label>
                    <div className="col-sm-5">
                    <input onChange={(e) => this.handleOnChange(e)} value={this.state.paymentDate} type="datetime-local" id="paymentDate" className="form-control" placeholder="Date" />
                    </div>
                </div>


                <div className="form-group row">
                    <label htmlFor="type" className="col-sm-1 col-form-label">Type</label>
                    <div className="col-sm-5">
                    <select id="type" onChange={(e) => this.handleOnChange(e)} value={this.state.type} className="form-control">
                         <option default></option>
                         <option>Deposit</option>
                         <option>Withdrawal</option>                         
                    </select>

                    </div>
                </div>


                <div className="form-group row">
                    <label htmlFor="amount" className="col-sm-1 col-form-label">Amount</label>
                    <div className="col-sm-5">
                    <input type="number" onChange={(e) => this.handleOnChange(e)} value={this.state.amount} id="amount" className="form-control" placeholder="Amount" />
                    </div>
                </div>

                <div className="form-group row">
                    <label htmlFor="custID" className="col-sm-1 col-form-label">Cust Id</label>
                    <div className="col-sm-5">
                    <input type="number" onChange={(e) => this.handleOnChange(e)} id="custID" value={this.state.custID} className="form-control" placeholder="Customer Id" />
                    </div>
                </div>

                <div className="form-group row">
                <div className="col-sm-1" > {this.state.error}  </div>
                <button type="submit" className="mt-3 col-sm-2 btn btn-primary">Submit</button>
                </div>

                
                {this.state.message?  <div className="alert alert-danger alert-dismissible fade show">
                            <strong>Error!</strong> {this.state.message} 
                           </div>:null}
            </form>
           )
            
    }
       
}

const mapStateToProps = (state) =>{
    return{
        error: state.payment.addError
    }
}
export default connect(mapStateToProps,null)(AddPaymentForm)