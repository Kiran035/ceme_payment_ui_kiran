import {ACTIVE_TAB as CONSTANTS} from '../constants/Constants'

const initialState = {
    active:1
}

export const navReducer = (state =initialState,action) =>{
    console.log(action.type)
    switch(action.type)
    {
        case CONSTANTS.ACTIVE_TAB:
            return {...state,active:action.data}
        default:
            return state
    }    
}