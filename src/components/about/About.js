import React from "react";
import { useSelector, useDispatch } from "react-redux";
import { checkAPIStatus } from "../store/StatusActions";
import {setActiveTab} from '../store/NavActions'
import  Loader from '../Loader'
const About = () => {
	const status = useSelector((state) => state.status);
	const dispatch = useDispatch();
    dispatch(setActiveTab(3))
	
	return (
		<>
			<button
				type="button"
				className="mt-5 btn btn-primary active"
				onClick={() => dispatch(checkAPIStatus())}
			>
				Refresh Status
			</button>

            
			{status.loading ? (
				<Loader />
			) : status.working ? (
				<div className=" mt-5 alert alert-success">
					<strong>Success!</strong> Payment API is Working!.
				</div>
			) : status.working===false ?(
				<div className="mt-5 alert alert-danger">
					<strong>Danger!</strong> Payment API is Not Working!.
				</div>
            ): null  //Initially No message displayed
        }
		</>
	);
};

export default About;
