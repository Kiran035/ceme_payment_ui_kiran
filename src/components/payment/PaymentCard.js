import React, {useState} from "react";
import PaymentDetails from './PaymentDetails'
import {deletePaymentDetails} from '../store/PaymentActions'
import { useDispatch } from "react-redux";

const PaymentCard = (props) => {
    const [visible, setVisible] = useState(false)
    const PAYMENT = props.payment
    
    
    const dispatch = useDispatch();
  	return (<>
		<div className="col-md-4">
			<div className="card mb-4 shadow-sm">
                <h5 className="card-header"> ID: {PAYMENT.id}</h5>
                <div className="card-body">
                    {
                        visible?<PaymentDetails {...PAYMENT}/>:null
                    }
                    <button className="card-link btn-primary" onClick={()=>setVisible(!visible)}>{visible?"Hide":"Show"}</button>
                    <button className="card-link" onClick={() => dispatch(deletePaymentDetails(PAYMENT))}>Delete</button>
                </div>
			</div>
		</div>
       
        
        </>
	);
};

export default PaymentCard;
