import axios from 'axios'
import {STATUS_CONSTANTS as CONSTANTS} from '../constants/Constants'

export const checkAPIStatus = () => {
    return (
        (dispatch) => {
            dispatch(whenStatusBegin())
            axios.get("http://localhost:8080/api/status")
            .then(
                (res) => dispatch(whenStatusSuccess()),
                (err) => dispatch(whenStatusFailure()),
            )
        }
    )
}

const whenStatusSuccess = () =>{
    return{
        type:CONSTANTS.STATUS_CHECK_SUCCESS,
    }
}

const whenStatusBegin = () =>{
    return{
        type:CONSTANTS.STATUS_CHECK_BEGIN,
    }
}

const whenStatusFailure = (err) =>{
    return{
        type:CONSTANTS.STATUS_CHECK_FAILURE,
    }
}