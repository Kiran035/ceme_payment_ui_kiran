import React, {useEffect} from 'react'
import AppRouter from './routers/AppRouter'
import NavBar from './NavBar'
import { useDispatch } from "react-redux";
import { BrowserRouter as Router } from 'react-router-dom'
import {fetchPaymentDetails} from '../components/store/PaymentActions'
import {checkAPIStatus} from '../components/store/StatusActions'
const App = () => {

    const dispatch = useDispatch();
        useEffect(() => {
            dispatch(fetchPaymentDetails()); 
            dispatch(checkAPIStatus())
        }); 

        
    return(
       <AppRouter />
        )
}

export default App