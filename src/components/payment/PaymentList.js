import React, {useState, useEffect} from 'react'
import PaymentCard from './PaymentCard'
import { useSelector } from "react-redux";
import Loader from '../Loader'
import Filter from './SearchFilter'
// import { useDispatch } from "react-redux";
// import {fetchPaymentDetails} from '../store/PaymentActions'
const PaymentList = () =>
{
    const paymentDetails = useSelector((state) => state.payment.paymentDetails)
    const paymentLoading = useSelector((state) => state.payment.loading)
    const paymentError = useSelector((state) => state.payment.error)
    
    let [filtereData, applyFilter] = useState([paymentDetails])

    //Filtering logic
    useEffect(()=>{
        applyFilter(paymentDetails)
    },[paymentDetails])
   
    return(<>
        <Filter data={[...paymentDetails]} applyFilter={applyFilter}/>
        <div className="album py-5 bg-light mt-5">
            <div className="container">
                <div className="row">

                {
                    paymentLoading ? <Loader /> :
                    
                    paymentError!==null ? 
                        <div className="alert alert-danger alert-dismissible fade show">
                            <strong>Error!</strong> {paymentError.message} 
                            {/* <button type="button" className="btn btn-link">Refresh</button>  */}
                        </div>
                        :

                        filtereData.map((payment,i) =>(
                    <PaymentCard key={i} payment={payment}/>
                    ))
                }
                </div>
            </div>
        </div>
    </>   
    )
}

export default PaymentList
