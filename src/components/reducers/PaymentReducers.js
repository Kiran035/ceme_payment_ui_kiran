import {PAYMENT_CONSTANTS as CONSTANTS}  from '../constants/Constants'

const initialState = {
    paymentDetails : [],
    error:null,
    loading:false,
    deleteError:null,
    addError:null
}

export const paymentReducer = (state =initialState,action) =>{
   console.log(action.type)
    switch(action.type)
    {
        case CONSTANTS.PAYMENT_FETCH_BEGIN:
            return {...state,
                    error:null,
                    loading:true,
                    addError:null,
                    deleteError:null}
        case CONSTANTS.PAYMENT_FETCH_FAILURE:
            return {...state,error:action.data,loading:false}  
        case CONSTANTS.PAYMENT_ADD_BEGIN:
            return {...state,addError:null} 
        case CONSTANTS.PAYMENT_FETCH_SUCCESS:
             return {...state,paymentDetails:action.data, error:null,loading:false}
        case CONSTANTS.PAYMENT_ADD_FAILURE:
            return {...state,addError:action.data}
        case CONSTANTS.PAYMENT_DELETE_SUCCESS:
             let newPaymentList = state.paymentDetails
             newPaymentList = newPaymentList.filter(item => item !== action.data)
             return {...state,paymentDetails:newPaymentList}
        case CONSTANTS.PAYMENT_DELETE_FAILURE:
            return {...state,deleteError:action.data}
        default:
            return state
    }    
}