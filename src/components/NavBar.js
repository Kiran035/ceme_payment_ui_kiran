import React from 'react'
import {Link} from 'react-router-dom' 
import { useDispatch,useSelector } from "react-redux";
import {setActiveTab} from './store/NavActions'

const  NavBar = () => {
    
    
    const activeTab = useSelector((state) => state.navMenu.active);
	//const [activeTab,setActiveTab] = useState(1)
    const dispatch = useDispatch();
   
    const activeTabCLass = "nav-link active"
    const nonActiveTabCLass = "nav-link"
    return(
        <div className="card-header">
            <ul className="nav nav-tabs card-header-tabs">
                <li className="nav-item">
                    <Link className={activeTab===1?activeTabCLass: nonActiveTabCLass} to="/" onClick={() => dispatch(setActiveTab(1))}>Home</Link>
                </li>
                <li className="nav-item">
                    <Link className={activeTab===2?activeTabCLass: nonActiveTabCLass} to="/addPayment" onClick={() => dispatch(setActiveTab(2))}>Add Payment</Link>
                </li>
                <li className="nav-item">
                    <Link className={activeTab===3?activeTabCLass: nonActiveTabCLass} to="/about" onClick={() => dispatch(setActiveTab(3))}>About</Link>
                </li>
            </ul>
        </div>
        
    )
}


export default NavBar