import {STATUS_CONSTANTS as CONSTANTS}  from '../constants/Constants'

const initialState = {
}

export const statusReducer = (state =initialState,action) =>{
    
    switch(action.type)
    {
        case CONSTANTS.STATUS_CHECK_BEGIN:
            return {...state,loading:true}
        case CONSTANTS.STATUS_CHECK_FAILURE:
            return {...state,loading:false,working:false}    
        case CONSTANTS.STATUS_CHECK_SUCCESS:
             return {...state,loading:false,working:true}
        default:
            return state
    }    
}