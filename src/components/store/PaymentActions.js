import axios from 'axios'
import {PAYMENT_CONSTANTS as CONSTANTS} from '../constants/Constants'
export const fetchPaymentDetails = () =>{
    return (dispatch,getState) => {
        dispatch(fetchPaymentDetailsBegin())
        setTimeout(()=>(
            axios.get("http://localhost:8080/api/payment/all").then(
                (res) =>{
                    dispatch(fetchPaymentDetailsSuccess(res.data))
                },
                (err) =>{
                    dispatch(fetchPaymentDetailsFailure(err))
                }
            )
        ),3000)
        }
    }

   

    
    export const addPaymentDetails = (payment) =>{
        return (dispatch,getState) => {
            dispatch(addPaymentDetailsBegin())
            return axios.post("http://localhost:8080/api/payment/save", payment).then(
            (res) =>{
                dispatch(fetchPaymentDetails())
              },
            (err) =>{
                dispatch(addPaymentDetailsFailure(err))
                throw err
            }
        )
    }

    
}
    

    const fetchPaymentDetailsSuccess = (data) =>{
        return{
            type:CONSTANTS.PAYMENT_FETCH_SUCCESS,
            data:data
        }
    }

    const fetchPaymentDetailsBegin = () =>{
        return{
            type:CONSTANTS.PAYMENT_FETCH_BEGIN,
    }
}

const fetchPaymentDetailsFailure = (err) =>{
    return{
        type:CONSTANTS.PAYMENT_FETCH_FAILURE,
        data:{message:"Unable to fetch payment details, Try later..."}
    }
}

export const addPaymentDetailsBegin = (err) =>{
    return{
        type:CONSTANTS.PAYMENT_ADD_BEGIN,
    }
}
const addPaymentDetailsFailure = (err) =>{
    return{
        type:CONSTANTS.PAYMENT_ADD_FAILURE,
        data:{message:"Unable to add payment details, Try later..."}
    }
}

export const deletePaymentDetails = (payment) =>{
    console.log(payment)
    return (dispatch,getState) => {
        return axios.post("http://localhost:8080/api/payment/delete", payment).then(
        (res) =>{
            dispatch(deletePaymentDetailsSuccess(payment))
          },
        (err) =>{
            dispatch(deletePaymentDetailsFailure(err))
            throw err
        }
    )
}}

const deletePaymentDetailsFailure = (err) =>{
    return{
        type:CONSTANTS.PAYMENT_DELETE_FAILURE,
        data:{message:"Unable to delete payment details, Try later..."}
    }
}

const deletePaymentDetailsSuccess = (payment) =>{
    return{
        type:CONSTANTS.PAYMENT_DELETE_SUCCESS,
        data:payment
    }
}
